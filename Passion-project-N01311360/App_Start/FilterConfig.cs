﻿using System.Web;
using System.Web.Mvc;

namespace Passion_project_N01311360
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
