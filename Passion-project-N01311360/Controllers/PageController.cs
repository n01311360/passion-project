﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passion_project_N01311360.Models;
using System.Diagnostics;

namespace Passion_project_N01311360.Controllers
{
    public class PageController : Controller
    {
        private Passion_projectContext db = new Passion_projectContext();

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult New()
        {
            return View(db.Genres.ToList());
        }

        [HttpPost]
        public ActionResult Create(string PageTitle_New, string PageContent_New, int? Page_New)
        {
            string query = "insert into pages (PageTitle, PageContent, show_ShowId)" +
                "values (@titile, @content,@bid)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@title", PageTitle_New);
            myparams[1] = new SqlParameter("@content", PageContent_New);
            myparams[2] = new SqlParameter("@bid", Page_New);

            db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);
            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            string query = "select * from pages where pageid  =@id";

            Debug.WriteLine(query);

            return View(db.Pages.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        public ActionResult Edit(int? id)
        {
            string query = "select * from pages where pageid =@id";
            return View(db.Pages.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());

        }
        [HttpPost]
        public ActionResult Edit(int? id, string PageTitle, string PageContent, int? PageGenres, int?[) Removed_PageGenres)
        {
            if ((id == null) || (db.Pages.Find(id) == null))
            {
                return HttpNotFound();

            }
    }

    // GET: Page/Edit/5
    public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Page/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Page/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Page/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
