﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Mvc;
using Passion_project_N01311360.Models;
using System.Diagnostics;


namespace Passion_project_N01311360.Controllers
{
    public class genreController : Controller
    {
        Passion_projectContext db = new Passion_projectContext();

        public ActionResult Index()
        {
            return RedirectToAction("list");
        }

        public ActionResult List()
        {
            string query = "select from Genres";
            IEnumerable<Genre> genres = db.Genres.SqlQuery(query);

            return View(genres);
            //IEnumerable<Genre> genre = db.Genre.ToList();
            //return View(genre);
        }

        public ActionResults New()
        {


            return View();

        }
        [HttpPost]
        public ActionResult Create(string GenreType_new, string genreColor_new)
        {
            string query = "insert into genres (GenreType, GenreColor)" + "values (@name, @color)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", GenreType_new);
            myparams[1] = new SqlParameter("@color", GenreColor_new);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("list");
        }


        public class ActionResults Edit(int? id)
        {
            if ((id == null) || (db.Genres.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from Genres where genreis=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Genre myGenre = db.Genres.SqlQuery(query, param).FirstOrDefault();
            return View(myGenre);
        }

        //View file called List 
        public ActionResult Edit(int? id, string GenreType, string GenreColor)

        {
            if ((id == null) || (db.Genre.Find(id) == null)) {
                return HttpNotFound();
            }
            string query = "update genres set GenreType=@name, GenereColor=@color" + "where genereis=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", GenreType);
            myparams[1] = new SqlParameter("@color", GenreColor);
            myparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams params);
            return RedirectToAction("display" + id);
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Genre.Find(id) == null))
    }
           return HttpNotFound();
    }
    string query = "delete from genre where generid=@id";
    Sqlparameter param = new Sqlparameter("@id", id);
    bd.Database.ExecuteSqlCommand(query, param);
    return View("list");
}

public ActionResult show(int? id)
{
    if ((id == null) || (db.Genre.Find(id) == null))
    {
        return HttpNotFound();
    }
    string query = "select * fron Genre where genreid=@id";
    SqlParameter param = new SqlParameter("@id", id);

    Genre genretoshow = db.Genre.Find(id);

    return View(genretoshow);
}










}

       