﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Passion_project_N01311360.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "What do you wanna watch? ";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Find a show";

            return View();
        }
    }
}