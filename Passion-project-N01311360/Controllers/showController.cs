﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Passion_project_N01311360.Models;
using System.Diagnostics;

namespace Passion_project_N01311360.Controllers
{
    public class ShowController : Controller
    {
        private Passion_projectContext db = new Passion_projectContext();
        // GET: show
        public ActionResult Index()
        {
            return View(db.Shows.ToList());
        }

        // GET: show/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Show show = db.Shows.Find(id);
            if (show == null)
            {
                return HttpNotFound();
           
            }
            return View(show);
        }

        // GET: show/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: show/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "ShowId,ShowTitle,ShowType")] Show show)
        {
            if (ModelState.IsValid)
            {
                db.Show.Add(show);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
                return View(show);
            
        }

        // GET: show/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                Show show = db.Shows.Find(id);
                if (show == null)
            {
                return HttpNotFound();
            }
                return View(show);
        }

        }

        // GET: show/Delete/5
        public ActionResult Delete(int id)
        {
        if (id == null)
        {
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
            Show show = db.Shows.Find(id);
            if (show == null)
        }
            return HttpNotFound();
        }
            return View(show);
     }

        // POST: show/Delete/5
        [HttpPost, ActionName("Delete")]
        public ActionResult Delete(int id, FormCollection collection)
        {

            Show show = db.Show.Find(id);
            db.Show.Remove(show);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       
       
    }
}
