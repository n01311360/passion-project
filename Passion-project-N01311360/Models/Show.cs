﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Passion_project_N01311360.Models
{
    public class Show
    {
        //primary key showid
        [Key,ScaffoldColumn(false)]
        public int Showid { get; set; }
        //showname
        [Required, StringLength(255), Display(Name = "Show Title")]
        public string ShowTitle { get; set; }
        //discription
        [StringLength(int.MaxValue), Display(Name = "Show Description")]
        public string ShowDescription { get; set; }
        //genre
        public virtual Genre genre { get; set; }

    }
}