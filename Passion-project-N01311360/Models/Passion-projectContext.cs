﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;

namespace Passion_project_N01311360.Models
{
    public class Passion_projectContext
    {
        public object Shows { get; internal set; }
        public object Genres { get; internal set; }
        public object Database { get; internal set; }
        public object Pages { get; internal set; }

        public class BlogCMSContext : DbContext
        {

            public BlogCMSContext()
            {

            }

            public DbSet<Genre> Genre { get; set; }
            public DbSet<Show> Shows { get; set; }

        }
    }
}
}