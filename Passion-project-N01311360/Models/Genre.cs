﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Passion_project_N01311360.Models
{
    public class Genre
    {
        //primary key genreID
        [Key,ScaffoldColumn(false)]
        public int GenreID { get; set; }

        [Required, StringLength(255), Display(Name ="Genre List")]
        public string GenreTitle { get; set; }
        //genrename
        //collection of shows
        public virtual ICollection<Show> Shows { get; set; }
    }
}