﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Passion_project_N01311360.Models
{
    public class Page
    {
        [Key, ScaffoldColumn(false)]
        public int PageId { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string PageTitle { get; set; }

        public virtual Show show { get; set; }

        public virtual ICollection<Genre> Genres { get; set; }

    }       
}
